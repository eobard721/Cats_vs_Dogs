import shutil
import uuid
import os
import time
import torch
from torchvision import transforms
from utils.model import ResNet18,Residual
from utils.model_d_c import Inception,MyGoogLeNet

# Constant
device="cuda" if torch.cuda.is_available() else "cpu"
db="static/result.txt"
user_info="static/users.txt"

# model="pretrain/mask_nomask_resnet18.pth"
# classes = ["戴了口罩", "没戴口罩"]
# mask_normalize=transforms.Normalize([0.19880751, 0.18449761, 0.17923173], [0.0434632, 0.03983969, 0.0389046])

model="pretrain/dog_cat.pth"
classes=["猫","狗"]
animal_normalize=transforms.Normalize([0.16277768, 0.1517874, 0.13888113], [0.02913429, 0.02624604, 0.02397004])
model_upload_folder = "./pretrain"
img_folder="./templates/img/"

# 注册
def register(username, password):
    # 将新用户的信息追加写入到 users.txt 文件中
    with open(user_info, 'a') as file:
        file.write(f"{username}={password}\n")

# 用户验证
def authenticate(username, password):
    # 从 users.txt 文件中读取用户名和密码
    with open(user_info, 'r') as file:
        for line in file:
            stored_username, stored_password = line.strip().split('=')
            if username == stored_username and password == stored_password:
                return True
    return False

# 检查是否存在相同的用户名
def check_existing_username(username):
    # 打开文件，使用 'r' 模式表示读取模式
    with open(db, 'r') as file:
        # 逐行读取文件内容
        for line in file:
            stored_username, _ = line.strip().split('=')
            if username == stored_username:
                return True
    return False

# 加载ResNet18模型
def load_resnet18(model_path):
    model = ResNet18(Residual).to(device)
    model.load_state_dict(torch.load(model_path,map_location=device))
    return model

# 加载GoogLeNet
def load_GoogLeNet(model_path):
    model=MyGoogLeNet(Inception).to(device)
    model.load_state_dict(torch.load(model_path,map_location=device))
    return model

# 推理图片
def inference_img(img,model):
    print("inference start")
    start=time.time()
    # 归一化处理
    normalize =animal_normalize
    # 数据集处理方法
    transform = transforms.Compose([transforms.Resize((224, 224)), transforms.ToTensor(), normalize])
    # 应用到图片
    img = transform(img)
    # 添加批次
    img = img.unsqueeze(0).to(device)

    with torch.no_grad():
        model.eval()
        output = model(img)
        pre_lab = torch.argmax(output, dim=1)
        item = pre_lab.item()
    end = time.time()
    print(f"inference done! total use {end-start} seconds")
    return classes[item]

# 上传模型
def upload_model(uploaded_file):
    if not os.path.exists(model_upload_folder):
        os.makedirs(model_upload_folder)
    try:
        # 生成唯一的文件名
        filename = str(uuid.uuid4()) + os.path.splitext(uploaded_file.filename)[-1]
        destination = os.path.join(model_upload_folder, filename)
        # 使用 shutil.copy 将文件复制到目标文件夹，并重命名
        with open(destination, 'wb') as f:
            shutil.copyfileobj(uploaded_file, f)
        #   加载上传模型
        # model=load_resnet18(destination)
        model=load_GoogLeNet(destination)
        return '已成功上传，模型加载成功！',model
    except Exception as e:
        return '上传模型失败：{}'.format(str(e)),None

# 上传图片
def upload_img(img):
    if not os.path.exists(img_folder):
        os.makedirs(img_folder)
    try:
        # 生成唯一的文件名
        filename = str(uuid.uuid4()) + os.path.splitext(img.filename)[-1]
        destination = os.path.join(img_folder, filename)
        # 使用 shutil.copy 将文件复制到目标文件夹，并重命名
        with open(destination, 'wb') as f:
            shutil.copyfileobj(img, f)
        return filename
    except Exception as e:
        return  None

# 将检测结果保存到文件
def write_info(file_name,result):
    # 将检测信息追加写入到 db.txt 文件中
    with open(db, 'a') as file:
        file.write(f"{file_name}={result}\n")