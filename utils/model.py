import torch
from torch import nn
from torchsummary import summary

# 残差块
class Residual(nn.Module):
    def __init__(self,in_channels,out_channels,strides=1,has_1_conv=False):
        super(Residual, self).__init__()

        # 卷积
        self.conv1=nn.Conv2d(in_channels=in_channels,out_channels=out_channels,kernel_size=3,padding=1,stride=strides)
        self.conv2 = nn.Conv2d(in_channels=out_channels,out_channels=out_channels,kernel_size=3,padding=1)

        # BN:根据通道进行批量归一化
        self.bn1=nn.BatchNorm2d(out_channels)
        self.bn2 = nn.BatchNorm2d(out_channels)

        # utils*1卷积
        if has_1_conv:
            self.conv3=nn.Conv2d(in_channels=in_channels,out_channels=out_channels,kernel_size=1,stride=strides)
        else:
            self.conv3=None

        # ReLU激活函数
        self.ReLU=nn.ReLU()

    # 前向传播
    def forward(self,x):
        # 卷积 → 批量规范化 → 激活函数
        y = self.ReLU(self.bn1(self.conv1(x)))
        y = self.ReLU(self.bn2(self.conv2(y)))

        # 如果1*1卷积存在
        if self.conv3:
            x=self.conv3(x)

        return self.ReLU(y+x)

# 残差网络: 输入为3*224*224
class ResNet18(nn.Module):
    def __init__(self,Residual):
        super(ResNet18, self).__init__()

        self.b1=nn.Sequential(
            nn.Conv2d(in_channels=3,out_channels=64,kernel_size=7,padding=3,stride=2),
            nn.ReLU(),
            nn.BatchNorm2d(64),
            nn.MaxPool2d(kernel_size=3,padding=1,stride=2)
        )

        self.b2=nn.Sequential(
            Residual(in_channels=64,out_channels=64,strides=1,has_1_conv=False),
            Residual(in_channels=64,out_channels=64,strides=1,has_1_conv=False)
        )

        self.b3=nn.Sequential(
            Residual(in_channels=64, out_channels=128, strides=2, has_1_conv=True),
            Residual(in_channels=128, out_channels=128, strides=1, has_1_conv=False)
        )

        self.b4 = nn.Sequential(
            Residual(in_channels=128, out_channels=256, strides=2, has_1_conv=True),
            Residual(in_channels=256, out_channels=256, strides=1, has_1_conv=False)
        )

        self.b5 = nn.Sequential(
            Residual(in_channels=256, out_channels=512, strides=2, has_1_conv=True),
            Residual(in_channels=512, out_channels=512, strides=1, has_1_conv=False)
        )

        self.b6=nn.Sequential(
            # GAP全局平均池化层
            nn.AdaptiveAvgPool2d((1,1)),
            # 扁平化
            nn.Flatten(),
            # 全连接层: 这里为2分类
            nn.Linear(512,2)
        )

    # 前向传播
    def forward(self,x):
        x = self.b1(x)
        x = self.b2(x)
        x = self.b3(x)
        x = self.b4(x)
        x = self.b5(x)
        return  self.b6(x)


if __name__ == '__main__':
    device=torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model=ResNet18(Residual).to(device)
    print(summary(model,input_size=(3,224,224)))