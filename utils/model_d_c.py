import torch
from torch import nn
from torchsummary import summary

# Inception层
class Inception(nn.Module):
    """
        in_channels:输入通道
        p1_filters:输出通道
        p2_filters(第一步卷积核数量,第二步卷积核数量):输出通道
        p3_filters(第一步卷积核数量,第二步卷积核数量):输出通道
        p4_filters:输出通道
    """
    def __init__(self,in_channels,p1_filters,p2_filters,p3_filters,p4_filters):
        super(Inception, self).__init__()
        # 路线1: 1*1卷积
        self.p1_1=nn.Conv2d(in_channels=in_channels,out_channels=p1_filters,kernel_size=1)

        # 路线2: 1*1卷积 -> 3*3卷积
        self.p2_1=nn.Conv2d(in_channels=in_channels,out_channels=p2_filters[0],kernel_size=1)
        self.p2_2 = nn.Conv2d(in_channels=p2_filters[0], out_channels=p2_filters[1], kernel_size=3,padding=1)

        # 路线3: 1*1卷积 -> 5*5卷积
        self.p3_1 = nn.Conv2d(in_channels=in_channels, out_channels=p3_filters[0], kernel_size=1)
        self.p3_2 = nn.Conv2d(in_channels=p3_filters[0], out_channels=p3_filters[1], kernel_size=5, padding=2)

        # 路线4: 3*3最大池化 -> 1*1卷积
        self.p4_1 = nn.MaxPool2d(kernel_size=3,padding=1,stride=1)
        self.p4_2 = nn.Conv2d(in_channels=in_channels, out_channels=p4_filters, kernel_size=1)

        self.ReLU=nn.ReLU()

    # 前向传播
    def forward(self,x):
        # 计算路径1
        p1 = self.ReLU(self.p1_1(x))
        # 计算路径2
        p2 = self.ReLU(self.p2_2(self.ReLU(self.p2_1(x))))
        # 计算路径3
        p3 = self.ReLU(self.p3_2(self.ReLU(self.p3_1(x))))
        # 计算路径4
        p4 = self.ReLU(self.p4_2(self.p4_1(x)))

        # 按通道融合
        # p1~p4都是(batch_size,不同的通道数,w,h)的tensor,只是通道数不同,按dim=1即通道数融合
        return torch.cat(tensors=(p1,p2,p3,p4),dim=1)


# 输入224*224*3的图像
class MyGoogLeNet(nn.Module):
    def __init__(self,Inception):
        super(MyGoogLeNet, self).__init__()

        self.b1=nn.Sequential(
            #  [-1, 64, 112, 112]
            nn.Conv2d(in_channels=3,out_channels=64,kernel_size=7,stride=2,padding=3),
            nn.ReLU(),
            #  [-1, 64, 56, 56]
            nn.MaxPool2d(kernel_size=3,stride=2,padding=1)
        )

        self.b2=nn.Sequential(
            #  [-1, 64, 56, 56]
            nn.Conv2d(in_channels=64, out_channels=64, kernel_size=1),
            nn.ReLU(),
            #  [-1, 192, 56, 56]
            nn.Conv2d(in_channels=64, out_channels=192, kernel_size=3,padding=1),
            nn.ReLU(),
            # [-1, 192, 28, 28]
            nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        )

        self.b3=nn.Sequential(
            Inception(192,64,(96,128),(16,32),32),
            Inception(256,128,(128,192),(32,96),64),
            nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        )

        self.b4=nn.Sequential(
            Inception(480,192,(96,208),(16,48),64),
            Inception(512,160,(112,224),(24,64),64),
            Inception(512, 128, (128, 256), (24, 64), 64),
            Inception(512, 112, (128, 288), (32, 64), 64),
            Inception(528, 256, (160, 320), (32, 128), 128),
            nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        )

        self.b5 = nn.Sequential(
            Inception(832, 256, (160, 320), (32, 128), 128),
            Inception(832, 384, (192, 384), (48, 128), 128),
            # 全局平均池化层：将(batch_size,channels,w,h)的张量变为(batch_size,channels,1,1)
            # [-1, 1024, 1, 1]
            nn.AdaptiveAvgPool2d((1,1)),
            # 扁平化：[-1, 1024]
            nn.Flatten(),
            # 输出2分类任务
            nn.Linear(1024,2)
        )

        """
            对于VGG16,GoogleNet这类深层次的网络，权重不初始化、batch_size不合适
            都会导致训练几十轮之后不收敛，并且loss值和acc值效果不好。
        """
        # 遍历网络中的每个模块的参数
        for m in self.modules():
            # 如果是卷积层的实例
            if isinstance(m, nn.Conv2d):
                # 对该卷积层的权重进行初始化,使用输出连接数来调整权重的标准差,使用relu作为非线性函数,使其更快的在训练过程中收敛
                nn.init.kaiming_normal_(m.weight,mode="fan_out", nonlinearity="relu")
                # 并对其偏置设为0
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)

            # 如果是fc的实例
            elif isinstance(m, nn.Linear):
                # 对全连接层的权重按照均值为0，标准差为0.01的正态分布进行初始化
                nn.init.normal_(m.weight, 0, 0.01)
                # 并对其偏置设为0
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)

    # 前向传播
    def forward(self,x):
        x=self.b1(x)
        x=self.b2(x)
        x=self.b3(x)
        x=self.b4(x)
        x=self.b5(x)
        return x

if __name__ == '__main__':
    device=torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model=MyGoogLeNet(Inception).to(device)
    summary(model,(3,224,224))