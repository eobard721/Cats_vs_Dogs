# 猫狗识别

> **developed by eobard thawne**



## 1. 依赖安装

在cmd中安装以下依赖环境

```bash
pip install numpy
pip install pillow
pip install flask
pip install torch==1.10.0+cpu torchvision==0.11.0+cpu torchaudio==0.10.0 -f https://download.pytorch.org/whl/torch_stable.html
pip install torchsummary
```




## 2.运行app.py文件

![image-20240510091236277](/README_imgs/image-20240510091236277.png)

![image-20240510091358214](/README_imgs/image-20240510091358214.png)







## 3.登录 or 注册

> **默认账号admin，密码为123456**

![image-20240510091333999](/README_imgs/image-20240510091333999.png)

![image-20240510091422123](/README_imgs/image-20240510091422123.png)











## 4.页面效果

* 首先加载已有模型

![image-20240510091442366](/README_imgs/image-20240510091442366.png)





* **上传`猫或狗`的图片**

> **切记：只能上传猫和狗的图片，该项目以猫狗分类为例**

![image-20240510091455144](/README_imgs/image-20240510091455144.png)





* 上传

![image-20240510091504837](/README_imgs/image-20240510091504837.png)

![image-20240510091512423](/README_imgs/image-20240510091512423.png)

![image-20240510091518582](/README_imgs/image-20240510091518582.png)





* 点击开始检测按钮

![image-20240510091525107](/README_imgs/image-20240510091525107.png)





### 注意

> **该模型并未经过大量的训练，正确率约为 72~80%，可在历史检测结果中可以看到有错误的检测**



## 5.查看历史检测结果

![image-20240510091530683](/README_imgs/image-20240510091530683.png)