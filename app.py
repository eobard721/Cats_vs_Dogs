import io

from PIL import Image
from flask import Flask, render_template, request, redirect, url_for, jsonify, send_from_directory
from utils import commons as utils
app = Flask(__name__)

# 登录页面
@app.route('/')
def index():
    return render_template('login.html')

# 登录页面逻辑
@app.route('/login', methods=['POST'])
def login():
    username = request.form['username']
    password = request.form['password']
    if utils.authenticate(username, password):
        return redirect(url_for('main'))  # 登录成功后跳转到主页面
    else:
        return render_template('login.html', error="登录失败！请检查你的账号和密码")

    return redirect(url_for('index'))


# 主页面
@app.route('/main')
def main():
    return render_template('main.html')

# 检查是否存在用户名
@app.route('/check', methods=['GET'])
def check_existing_username():
    username = request.args.get('username')
    if utils.check_existing_username(username):
        return jsonify({'error': 'error'})
    return jsonify({'error': ""})

# 注册页面
@app.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        utils.register(username, password)
        return redirect(url_for('index'))
    else:
        return render_template('register.html')

# 模型文件
model=None
# 图片文件
filename=None

# 加载已有模型
@app.route('/load_existing_model', methods=['GET'])
def load_existing_model():
    global model
    # model=utils.load_resnet18(utils.model)
    model=utils.load_GoogLeNet(utils.model)
    return '已成功加载已有模型'

# 上传模型
@app.route('/upload_existing_model', methods=['POST'])
def upload_existing_model():
    global model
    uploaded_file = request.files['model_file']
    res,model=utils.upload_model(uploaded_file)
    return res

# 上传图片
@app.route('/upload_image', methods=['POST'])
def upload_image():
    file = request.files['image_file']
    global filename
    filename = utils.upload_img(file)
    if filename:
        return f'/img/{filename}'
    return None

# 回显图片
@app.route('/img/<filename>')
def uploaded_image(filename):
    return send_from_directory(utils.img_folder, filename)

# 开始检测图片
@app.route('/start_detection', methods=['POST'])
def start_detection():
    # 从请求中获取图像文件
    image_file = request.files['image_file']
    # 将图像文件读取为 PIL Image 对象
    image = Image.open(io.BytesIO(image_file.read()))
    # 开始推理
    result=utils.inference_img(image,model)
    detection_result = f"检测结果：{result}"
    utils.write_info(filename,detection_result)
    # 返回检测结果
    return detection_result

@app.route('/history')
def history():
    # 读取 db.txt 文件内容
    with open(utils.db, 'r') as file:
        lines = file.readlines()
    # 提取图片文件名和标签信息
    image_data = []
    for line in lines:
        filename, label = line.strip().split('=')
        image_data.append({'filename': filename, 'label': label})

    # 将图片文件名和标签信息传递给 history.html 模板
    return render_template('history.html', image_data=image_data)


# todo:将检测过的图片用一个页面展示出来，并在下面显示检测结果
if __name__ == '__main__':
    app.run(debug=True)

